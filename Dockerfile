FROM codercom/code-server:3.5.0

USER root 

# Install Go
RUN curl -sL https://dl.google.com/go/go1.15.linux-amd64.tar.gz | tar -zx -C /usr/local

# Setup User Go Environment
ENV PATH "${PATH}:/usr/local/go/bin:/home/coder/go/bin"
ENV GOPATH "$HOME/go"
ENV GO111MODULE=on

RUN go get -u github.com/go-delve/delve/cmd/dlv 
RUN go get golang.org/x/tools/gopls@latest

USER coder

